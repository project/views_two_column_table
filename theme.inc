<?php

/**
 * Display the view as a two column table.
 */
function template_preprocess_views_view_two_column_table(&$vars) {
  template_preprocess_views_view_table($vars);
}
